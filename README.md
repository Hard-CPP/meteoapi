# README #

A simple weather R&D application powered by Symfony 5 and using the openweathermap API. The application shows you the weather of major French cities and allows you to looking for the weather of any city arround the world. Feel free to suggest any ideas for futur improvements.

![screenshot_1](https://i.postimg.cc/T3jWp6yH/1.png)