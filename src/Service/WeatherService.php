<?php

namespace App\Service;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class WeatherService
{
    private $client;
    private $apiKey;

    public function __construct($apiKey)
    {
        $this->client = HttpClient::create();
        $this->apiKey = $apiKey;
    }

    /**
     * @param array $weatherData
     * @return array
     */
    public function getWeather(array $weatherData) : array
    {
        try {
            return $this->client->request(
                'GET',
                'https://api.openweathermap.org/data/2.5/weather?q='.$weatherData['city'].','.$weatherData['country'].'&appid='.$this->apiKey.'&units=metric'.'&lang=fr'
            )->toArray();
        } catch (ClientExceptionInterface $e) {
            $e->getMessage();
        } catch (DecodingExceptionInterface $e) {
            $e->getMessage();
        } catch (RedirectionExceptionInterface $e) {
            $e->getMessage();
        } catch (ServerExceptionInterface $e) {
            $e->getMessage();
        } catch (TransportExceptionInterface $e) {
            $e->getMessage();
        }

        return [];
    }
}
