<?php

namespace App\Controller;

use App\Entity\City;
use App\Form\WeatherFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\WeatherService;

class WeatherController extends AbstractController
{
    private $weatherService;

    /**
     * WeatherController constructor.
     * @param WeatherService $weather
     */
    public function __construct(WeatherService $weather)
    {
        $this->weatherService = $weather;
    }

    /**
     * @Route("/{city}/{country}", name="weather")
     * @param Request $request
     * @param String $city
     * @param String $country
     * @return Response
     */
    public function index(Request $request, String $city = "toulouse", String $country = "fr") : Response
    {
        $form = $this->createForm(WeatherFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $city = $form['city_field']->getData();
            $country = $form['country_field']->getData();
        }

        return $this->render('weather/index.html.twig', [
            'form' => $form->createView(),
            'weather' => $this->weatherService->getWeather([
                "city" => $city,
                "country" => $country
            ])
        ]);
    }
}
