<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class WeatherFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('city_field', TextType::class, [
                'constraints' => new Length([
                    'min' => 3
                ]),
                'attr' => [
                    'class' => 'form-control mb-2',
                    'placeholder' => 'Rechercher ma ville...'
                ],
                'label' => false
            ])
            ->add('country_field', HiddenType::class, [
                'constraints' => new Length([
                    'min' => 2,
                    'max' => 2
                ]),
                'attr' => [
                    'class' => 'form-control mb-2 ',
                    'placeholder' => 'Code du pays...'
                ],
                'label' => 'code du pays pour une recherche hors de france (ex: "es" pour une recherche en Espagne)',
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([]);
    }
}
